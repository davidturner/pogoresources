var Discord = require('discord.io');
var logger = require('winston');
var auth = require('./auth.json');
const pokemon = require('pokemon');
var about = {
    game: {
        name: '!botinfo'
    }
};

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});
bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as %s - %s\n', bot.username, bot.id);
    // logger.info(bot.username + ' - (' + bot.id + ')');
    bot.setPresence(about);
    logger.info('Currently playing: ' + about.game.name);
});
bot.on("disconnect", function(err, errcode) {
    logger.log('Disconnected from Discord...');
    bot.connect();
});

bot.on('message', function (user, userID, channelID, message, evt) {

    // console.log(userID);
    // console.log(message.guild.roles.get(userID));
    // console.log(message);

    if (message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];

        args = args.splice(1);
        try {
            switch(cmd) {
                case 'botinfo':





                    bot.sendMessage({
                        to: channelID,
                        embed: {
                            title: 'Bot Info',
                            description: "Everything I'm capable of doing:",
                            color: 0xcc0000,
                            thumbnail:
                                {
                                    url: 'https://cdn.discordapp.com/avatars/342266774428844035/b2adb63d0f12c9e66cec0e7a4848354c.png?size=256'
                                },
                            fields: [
                                {
                                    name: "!pokemon Mewtwo | !pokemon 150",
                                    value: 'Provides information about the chosen Pokemon based on their Name or their Pokedex Number.'
                                },
                                {
                                    name: "!raid Moltres",
                                    value: 'Provides information about the named raid boss, including best counters.'
                                },
                                {
                                    name: "!type Dark",
                                    value: 'Provides information about the Pokemon type based on the type entered.'
                                },
                                {
                                    name: "!egg 10",
                                    value: 'Provides information about the Pokemon that can be hatched from a given egg type.'
                                }
                            ]
                        }
                    });

                    break;
                case 'type':
                    var type = args[0];
                    var lowerType = type.toLowerCase();
                    var wordType = type.toProperCase();

                    var superEffective = '',
                        resistant = '',
                        notVeryEffective = '',
                        weakTo = '';

                    switch ( lowerType ) {
                        case 'normal':
                            superEffective = 'Absolutely Nothing';
                            resistant = '__Ghost__';
                            notVeryEffective = '__Ghost__, Rock, Steel';
                            weakTo = 'Fighting';
                            break;
                        case 'fighting':
                            superEffective = 'Dark, Ice, Normal, Rock, Steel';
                            resistant = 'Bug, Dark, Rock';
                            notVeryEffective = '__Ghost__, Bug, Fairy, Flying, Poision, Psychic';
                            weakTo = 'Fairy, Flying, Psychic';
                            break;
                        case 'flying':
                            superEffective = 'Bug, Fighting, Grass';
                            resistant = '__Ground__, Bug, Fighting, Grass';
                            notVeryEffective = 'Electric, Rock, Steel';
                            weakTo = 'Electric, Ice, Rock';
                            break;
                        case 'poison':
                            superEffective = 'Grass, Fairy';
                            resistant = 'Bug, Fairy, Fighting, Grass, Poison';
                            notVeryEffective = '__Steel__, Ghost, Ground, Poison, Rock';
                            weakTo = 'Ground, Psychic';
                            break;
                        case 'ground':
                            superEffective = 'Electric, Fire, Poison, Rock, Steel';
                            resistant = '__Electric__, Poison, Rock';
                            notVeryEffective = '__Flying__, Bug, Grass';
                            weakTo = 'Grass, Ice, Water';
                            break;
                        case 'rock':
                            superEffective = 'Bug, Fire, Flying, Ice';
                            resistant = 'Fire, Flying, Normal, Poison';
                            notVeryEffective = 'Fighting, Ground, Steel';
                            weakTo = 'Fighting, Grass, Ground, Steel, Water';
                            break;
                        case 'bug':
                            superEffective = 'Psychic, Grass, Dark';
                            resistant = 'Fighting, Grass, Ground';
                            notVeryEffective = 'Fighting, Fire, Flying, Ghost, Poison, Steel, Fairy';
                            weakTo = 'Fire, Flying, Rock';
                            break;
                        case 'ghost':
                            superEffective = 'Ghost, Psychic';
                            resistant = '__Normal__, __Fighting__, Bug, Poison';
                            notVeryEffective = '__Normal__, Dark';
                            weakTo = 'Ghost, Dark';
                            break;
                        case 'steel':
                            superEffective = 'Fairy, Ice, Rock';
                            resistant = '__Poison__, Bug, Dragon, Fairy, Flying, Grass, Ice, Normal, Psychic, Rock, Steel';
                            notVeryEffective = 'Electric, Fire, Steel, Water';
                            weakTo = 'Fighting, Fire, Ground';
                            break;
                        case 'fire':
                            superEffective = 'Bug, Grass, Ice, Steel';
                            resistant = 'Bug, Fairy, Fire, Grass, Ice, Steel';
                            notVeryEffective = 'Dragon, Fire, Rock, Water';
                            weakTo = 'Ground, Rock, Water';
                            break;
                        case 'water':
                            superEffective = 'Fire, Ground, Rock';
                            resistant = 'Fire, Ice, Steel, Water';
                            notVeryEffective = 'Dragon, Grass, Water';
                            weakTo = 'Electric, Grass';
                            break;
                        case 'grass':
                            superEffective = 'Ground, Rock, Water';
                            resistant = 'Electric, Grass, Ground, Water';
                            notVeryEffective = 'Bug, Dragon, Fire, Flying, Grass, Poison, Steel';
                            weakTo = 'Bug, Fire, Flying, Ice, Poison';
                            break;
                        case 'lightning':
                        case 'electric':
                            wordType = 'Electric';
                            superEffective = 'Flying, Water';
                            resistant = 'Electric, Flying, Steel';
                            notVeryEffective = '__Ground__, Dragon, Electric, Grass';
                            weakTo = 'Ground';
                            break;
                        case 'psychic':
                            superEffective = 'Fighting, Poison';
                            resistant = 'Fighting, Psychic';
                            notVeryEffective = '__Dark__, Psychic, Steel';
                            weakTo = 'Bug, Dark, Ghost';
                            break;
                        case 'ice':
                            superEffective = 'Dragon, Flying, Grass, Ground';
                            resistant = 'Ice';
                            notVeryEffective = 'Fire, Ice, Steel, Water';
                            weakTo = 'Fighting, Fire, Rock, Steel';
                            break;
                        case 'dark':
                            superEffective = 'Ghost, Poison';
                            resistant = '__Psychic__, Dark, Ghost';
                            notVeryEffective = 'Dark, Fighting, Fairy';
                            weakTo = 'Bug, Fighting, Fairy';
                            break;
                        case 'dragon':
                            superEffective = 'Dragon';
                            resistant = 'Electric, Fire, Grass, Water';
                            notVeryEffective = '__Fairy__, Steel';
                            weakTo = 'Dragon, Ice, Fairy';
                            break;
                        case 'fairy':
                            superEffective = 'Dark, Dragon, Fighting';
                            resistant = '__Dragon__, Bug, Dark, Fighting';
                            notVeryEffective = 'Fire, Poison, Steel';
                            weakTo = 'Poison, Steel';
                            break;
                    }

                    bot.sendMessage({
                        to: channelID,
                        embed: {
                            title: wordType + ' Type',
                            description: "The following is some information that should be useful when using/attacking " + wordType + ' type Pokemon',
                            color: 0xcc0000,
                            thumbnail:
                                {
                                    url: 'http://cdn.theinternetisfor.info/types/'+wordType+'.png'
                                },
                            fields: [
                                {
                                    name: "Super Effective Against:",
                                    value: superEffective
                                },
                                {
                                    name: "Resistant To",
                                    value: resistant
                                },
                                {
                                    name: "Not Very Effective Against",
                                    value: notVeryEffective
                                },
                                {
                                    name: "Weak to:",
                                    value: weakTo
                                }
                            ]
                        }
                    });
                    break;
                case 'pokemon':

                    var id = args[0];
                    var int = parseInt(args[0],10);
                    var isNum = (args[0] == parseInt(args[0],10) ? true : false);

                    if ( !isNum ) {
                        id = pokemon.getId(id.toProperCase());
                    }

                    var name = pokemon.getName(id);

                    var pokemonName = pokemon.getName(id);

                    bot.sendMessage({
                        to: channelID,
                        embed: {
                            title: pokemonName,
                            url: 'https://pokemongo.gamepress.gg/pokemon/' + id,
                            description: "You wanna know more about " + pokemonName + '? Click above to get everything you need to know.',
                            color: 0xcc0000,
                            thumbnail:
                                {
                                    url: 'http://pogonium.com/img/pokemon-sprites/'+pad(id, 3)+'.png'
                                    // url: 'https://img.pokemondb.net/artwork/'+name.toLowerCase()+'.jpg'
                                },
                        }
                    });

                    break;
                case 'raid':
                    var id = args[0].toProperCase();
                    var url = 'https://pokemongo.gamepress.gg/raid-boss-counters#';
                    var raids = {
                        // T2
                        'Muk': 216,
                        'Exeggutor': 239,
                        'Weezing': 246,
                        'ElectaBuzz': 269,
                        'Magmar': 270,
                        // T3
                        'Arcanine': 150,
                        'Alakazam': 163,
                        'Machamp': 166,
                        'Gengar': 227,
                        'Vaporeon': 278,
                        'Jolteon': 279,
                        'Flareon': 280,
                        // T4
                        'Venusaur': 30,
                        'Charizard': 33,
                        'Blastoise': 37,
                        'Rhydon': 248,
                        'Lapras': 275,
                        'Snorlax': 287,
                        'Tyranitar': 3546,
                        // T5
                        'Articuno':288,
                        'Zapdos': 289,
                        'Moltres': 290,
                        'Mewtwo': 294,
                        'Mew': 295,
                        'Raikou': 3521,
                        'Entei': 3526,
                        'Suicune': 3531,
                        'Lugia': 3551,
                        'Ho-Oh': 3556,
                        'Celebi': 3561,
                    };
                    var pokemonid = args[0];
                    var isNum = (args[0] == parseInt(args[0],10) ? true : false);

                    if ( !isNum ) {
                        pokemonid = pokemon.getId(pokemonid.toProperCase());
                    }

                    if ( raids[id] !== undefined ) {
                        bot.sendMessage({
                            to: channelID,
                            embed: {
                                title: id.toProperCase() + ' Counters',
                                url: url + raids[id],
                                color: 0xcc0000,
                                description: "You wanna take down a " + id.toProperCase() + '? Click above to get the latest tips on how to take this critter down.',
                                thumbnail:
                                    {
                                        url: 'http://pogonium.com/img/pokemon-sprites/'+pad(pokemonid, 3)+'.png'
                                    },
                            }
                        });
                    } else {
                        bot.sendMessage({
                            to: channelID,
                            embed: {
                                title: 'No Data Found',
                                color: 0xcc0000,
                                description: "Sorry about this, but we don't have a resource to direct you to about this raid boss."
                            }
                        });
                    }

                    break;
                case "egg":
                    var distance = args[0];
                    // args = args.splice(1);
                    var title = 'No Egg Matching this distance found';
                    var msg = 'Try using `!egg 2`, `!egg 5`, or `!egg 10` to get some actual eggs!';

                    switch (args[0]) {
                        case 2:
                        case '2':
                        case '2k':
                        case '2km':
                            title = '2k eggs can hatch:';
                            msg = ['Remoraid',
                                'Slugma',
                                'Misdreavus',
                                'Aipom',
                                'Togepi',
                                'Igglybuff',
                                'Cleffa',
                                'Pichu',
                                'Spinarak',
                                'Goldeen',
                                'Exeggcute',
                                'Krabby',
                                'Gastly',
                                'Slowpoke',
                                'Geodude',
                                'Machop',
                                'Abra',
                                'Diglett',
                                'Oddish',
                                'Nidoran Male',
                                'Nidoran Female',
                                'Ekans',
                                'Squirtle',
                                'Charmander',
                                'Bulbasaur'];
                            msg.sort();
                            msg = msg.join(', ');
                            break;
                        case 5:
                        case '5':
                        case '5k':
                        case '5km':
                            title = '5k eggs can hatch:';
                            msg = ['Magby',
                                'Elekid',
                                'Smoochum',
                                'Tyrogue',
                                'Stantler',
                                'Phanpy',
                                'Houndour',
                                'Mantine',
                                'Swinub',
                                'Teddiursa',
                                'Sneasel',
                                'Shuckle',
                                'Qwilfish',
                                'Snubbull',
                                'Gligar',
                                'Dunsparce',
                                'Pineco',
                                'Girafarig',
                                'Wobbuffet',
                                'Wooper',
                                'Yanma',
                                'Hoppip',
                                'Marill',
                                'Natu',
                                'Chinchou',
                                'Totodile',
                                'Cyndaquil',
                                'Chikorita',
                                'Kabuto',
                                'Omanyte',
                                'Eevee',
                                'Pinsir',
                                'Scyther',
                                'Staryu',
                                'Tangela',
                                'Rhyhorn',
                                'Koffing',
                                'Lickitung',
                                'Cubone',
                                'Voltorb',
                                'Drowzee',
                                'Onix',
                                'Shellder',
                                'Grimer',
                                'Seel',
                                'Magnemite',
                                'Ponyta',
                                'Tentacool',
                                'Poliwag',
                                'Growlithe',
                                'Psyduck',
                                'Paras',
                                'Vulpix'];
                            msg.sort();
                            msg = msg.join(', ');
                            break;
                        case 10:
                        case '10':
                        case '10k':
                        case '10km':
                            title = '10k eggs can hatch:';
                            msg = ['Larvitar',
                                'Miltank',
                                'Skarmory',
                                'Sudowoodo',
                                'Mareep',
                                'Dratini',
                                'Snorlax',
                                'Aerodactyl',
                                'Porygon',
                                'Lapras',
                                'Chansey'];
                            msg.sort();
                            msg = msg.join(', ');
                            break;
                    }
                    bot.sendMessage({
                        to: channelID,
                        embed: {
                            title: title,
                            color: 0xcc0000,
                            description: msg
                        }
                    });

                    break;
                default:
                    logger.info(evt);
                    var knex = require('knex')({
                        client: 'mysql',
                        connection: {
                            host     : 'theinternetisfor.info',
                            user     : 'theinter_pogouse',
                            password : '*%5b?I4_y6?c',
                            database : 'theinter_pogo',
                            charset  : 'utf8'
                        }
                    });

                    var bookshelf = require('bookshelf')(knex);

                    var Command = bookshelf.Model.extend({
                        tableName: 'pogo_commands'
                    });

                    // Command.where('command', cmd).fetch().then(function(response) {
                    //     console.log(response.get('response'));
                    // }).catch(function(err) {
                    //     console.error(err);
                    // });



                    // bot.sendMessage({
                    //     to: channelID,
                    //     embed: {
                    //         title: 'Buh?',
                    //         // url: url + raids[id],
                    //         color: 0xcc0000,
                    //         description: 'You got me all flustered',
                    //         // thumbnail:
                    //         //     {
                    //         //         url: 'http://pogonium.com/img/pokemon-sprites/'+pad(pokemonid, 3)+'.png'
                    //         //     },
                    //     }
                    // });
                    break;

            }
        }
        catch (e) {

            logger.info(e);

            bot.sendMessage({
                to: channelID,
                embed: {
                    title: 'Sorry, but something has gone wrong',
                    description: 'Sorry, but I couldn\'t understand what you were asking me to do. It\'s likely that there\'s a typo in a Pokemon name, so please double check and try again.',
                    color: 0xcc0000,
                }
            });

        }

    }
});